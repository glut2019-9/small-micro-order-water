/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 80030
Source Host           : localhost:3306
Source Database       : jeecg-boot-vue

Target Server Type    : MYSQL
Target Server Version : 80030
File Encoding         : 65001

Date: 2022-09-28 15:01:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for userpersonaldata
-- ----------------------------
DROP TABLE IF EXISTS `userpersonaldata`;
CREATE TABLE `userpersonaldata` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `name` varchar(200) NOT NULL COMMENT '用户名',
  `tickets` int NOT NULL COMMENT '水票',
  `location` varchar(200) DEFAULT NULL COMMENT '地址',
  `telephonenumber` varchar(32) NOT NULL COMMENT '电话号码',
  `integral` int NOT NULL COMMENT '积分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of userpersonaldata
-- ----------------------------
INSERT INTO `userpersonaldata` VALUES ('1574593913992388609', 'admin', '2022-09-27 10:56:58', null, null, 'A01', 'usertest', '0', '广西壮族自治区桂林市雁山区桂林理工大学', '15577477780', '0');

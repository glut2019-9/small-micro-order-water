/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 80030
Source Host           : localhost:3306
Source Database       : jeecg-boot-vue

Target Server Type    : MYSQL
Target Server Version : 80030
File Encoding         : 65001

Date: 2022-09-23 11:17:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for xiaoweiproduct
-- ----------------------------
DROP TABLE IF EXISTS `xiaoweiproduct`;
CREATE TABLE `xiaoweiproduct` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `name` varchar(200) NOT NULL COMMENT '名字',
  `price` decimal(10,2) NOT NULL COMMENT '价格',
  `descript` text COMMENT '介绍',
  `img` varchar(250) DEFAULT NULL COMMENT '图片',
  `stock` int NOT NULL COMMENT '库存',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of xiaoweiproduct
-- ----------------------------
INSERT INTO `xiaoweiproduct` VALUES ('1573121428811452418', 'admin', '2022-09-23 09:25:50', null, null, 'A01', '农夫山泉', '2.00', '<p>农夫山泉，清甜可口。</p>', '321de31bc6f86b873f5ed3280c0fa7bc_1663896348355.jpg', '300');
INSERT INTO `xiaoweiproduct` VALUES ('1573121559803760641', 'admin', '2022-09-23 09:26:21', null, null, 'A01', '娃哈哈桶装水', '7.00', '<p>娃哈哈桶装水，便宜实惠</p>', '111(2)_1663896379009.jpg', '100');
INSERT INTO `xiaoweiproduct` VALUES ('1573121680394194945', 'admin', '2022-09-23 09:26:50', null, null, 'A01', '百岁山桶装水', '15.00', '<p>百岁山桶装水，给你至尊的享受</p>', '222_1663896408804.jpg', '100');
INSERT INTO `xiaoweiproduct` VALUES ('1573122211346944002', 'admin', '2022-09-23 09:28:57', null, null, 'A01', '巴黎水Perrier', '8.00', '<p>巴黎进口矿泉水，味道不只是矿泉水。</p>', '333_1663896534231.jpg', '100');

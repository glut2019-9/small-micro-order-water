/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 80030
Source Host           : localhost:3306
Source Database       : jeecg-boot-vue

Target Server Type    : MYSQL
Target Server Version : 80030
File Encoding         : 65001

Date: 2022-09-28 15:02:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for xiaoweiproductwechet
-- ----------------------------
DROP TABLE IF EXISTS `xiaoweiproductwechet`;
CREATE TABLE `xiaoweiproductwechet` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `goods_id` int NOT NULL COMMENT '商品编号',
  `cat_id` int NOT NULL COMMENT '商品类别',
  `goods_name` varchar(200) NOT NULL COMMENT '商品名称',
  `goods_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `goods_number` int NOT NULL COMMENT '商品数量',
  `goods_big_logo` varchar(250) DEFAULT NULL COMMENT '大图片',
  `goods_small_logo` varchar(250) DEFAULT NULL COMMENT '小图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of xiaoweiproductwechet
-- ----------------------------
INSERT INTO `xiaoweiproductwechet` VALUES ('1574043681278365697', 'admin', '2022-09-25 22:30:33', null, null, 'A01', '1', '1', '娃哈哈矿泉水', '7.00', '100', '图片1_1664116228159.jpg', '211_1664116231395.jpg');
INSERT INTO `xiaoweiproductwechet` VALUES ('1574043807279452161', 'admin', '2022-09-25 22:31:02', null, null, 'A01', '2', '1', '怡宝饮用水', '8.00', '100', '图片2_1664116256682.jpg', '223_1664116260984.jpg');
INSERT INTO `xiaoweiproductwechet` VALUES ('1574043989844922370', 'admin', '2022-09-25 22:31:46', null, null, 'A01', '3', '1', '农夫山泉饮用水', '9.00', '100', '224_1664116296830.jpg', '图片3_1664116303476.jpg');

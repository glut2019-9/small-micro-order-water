/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 80030
Source Host           : localhost:3306
Source Database       : jeecg-boot-vue

Target Server Type    : MYSQL
Target Server Version : 80030
File Encoding         : 65001

Date: 2022-09-23 11:17:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for xiaoweiuserdata
-- ----------------------------
DROP TABLE IF EXISTS `xiaoweiuserdata`;
CREATE TABLE `xiaoweiuserdata` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `username` varchar(200) NOT NULL COMMENT '用户名',
  `password` varchar(200) NOT NULL COMMENT '密码',
  `telephonenumber` varchar(100) NOT NULL COMMENT '电话号码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of xiaoweiuserdata
-- ----------------------------
INSERT INTO `xiaoweiuserdata` VALUES ('1573125101104947202', 'admin', '2022-09-23 09:40:26', 'admin', '2022-09-23 09:43:23', 'A01', 'admin', '12345678', '15577477780');

/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 80030
Source Host           : localhost:3306
Source Database       : jeecg-boot-vue

Target Server Type    : MYSQL
Target Server Version : 80030
File Encoding         : 65001

Date: 2022-09-28 15:02:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for xiaoweiproductwatertickets
-- ----------------------------
DROP TABLE IF EXISTS `xiaoweiproductwatertickets`;
CREATE TABLE `xiaoweiproductwatertickets` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `name` varchar(200) NOT NULL COMMENT '商品名称',
  `price` decimal(10,2) NOT NULL COMMENT '金额',
  `descript` text COMMENT '介绍',
  `picture` varchar(250) DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of xiaoweiproductwatertickets
-- ----------------------------
INSERT INTO `xiaoweiproductwatertickets` VALUES ('1574316982528004097', 'admin', '2022-09-26 16:36:33', 'admin', '2022-09-26 16:42:30', 'A01', '娃哈哈桶装水', '300.00', '<p>买30桶，送15桶</p>', '211_1664181391812.jpg');
INSERT INTO `xiaoweiproductwatertickets` VALUES ('1574317538499776514', 'admin', '2022-09-26 16:38:45', 'admin', '2022-09-26 16:42:07', 'A01', '农夫山泉桶装水', '240.00', '<p>买20桶，送10桶</p>', '图片3_1664181522682.jpg');
INSERT INTO `xiaoweiproductwatertickets` VALUES ('1574318182241554433', 'admin', '2022-09-26 16:41:18', null, null, 'A01', '百岁山桶装水', '150.00', '<p>买10桶，送5桶</p>', '222_1664181676339.jpg');
INSERT INTO `xiaoweiproductwatertickets` VALUES ('1574319212459085825', 'admin', '2022-09-26 16:45:24', null, null, 'A01', '怡宝桶装水', '980.00', '<p>买50桶，送20桶外加一台饮水机</p>', '1234_1664181921645.jpg');
